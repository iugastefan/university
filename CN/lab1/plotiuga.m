function plotiuga(f,A,B)
  X = linspace(A,B,100);
  Y = f(X);
  plot(X,Y);
end
